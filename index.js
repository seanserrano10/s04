/*Demo*/
document.getElementById("btn-1").addEventListener('click', () => {
	alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
	paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize = "50px";
});

/*
Lesson Proper
	Topics
		- Introduction
		- Writing comments
		- Syntax and Statements
		- String values through variables
		- Peeking at variables' value through console
		- Data types
		- Functions

JavaScript
	- is a scripting language that enables you to make interactive web pages
	- it is one core technology of the World Wide Web
	- was originally intended for web browsers. However, they are now also integrated in 
	web servers through the use of Node.js

Uses of JavaScript
	- Web app development
	- Browser-based game development
	- Art creation
	- Mobile applications

Writing comments in JavaScript:

	// - single line comments (ctrl + /)
		- can only make comments in single line

	/*
		multi-line comments (ctrl + shift + /)
		comments in JS, much like CSS and HTML, are not read by the browser
		these commentts are often uswd to add notes and to add markers to your code
	*/

console.log("Hello, my name is Sean. I purple you.");

/*
	In JavaScript, we can see or log messages in our console.

	Consoles are part of our browser which will allow us to see or log messages, data, or information
	from our programming language
	
	For most browsers, console can be accessed through its developer tools in the
	console tab

	In fact, consoles in browsers allow us to add some JavaScript expression
*/

/*
Statements
	- are instructions or expressions that we add to our programming language which
	will then be communicated to our computers
	- statements in JavaScript commonly end in semi-colon(;) however, JavaScript has
	an implemented way of automatically adding semi-colons. Therefore, unlike other
	languages, JS does NOT require semi-colons
	- Semicolons in JS are mostly used to mark the end of the statement

Syntax
	- syntax in programming is a set of rules that describes how statements are
	properly made/constructed
	- lines/blocks of code must follow certain rules for it to work. Because you are
	not merely communicating with another human but communicating with a computer

Variables

	In HTML, elements are containers of other elements and text

	In JavaScript, variables are containers of data. A given name is used to describe
	that piece of data

	Variables also allows us to use or refer to data multiple times
*/

//num is the variable
//10 being the value/data
let num = 10;

console.log(6);
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);

/*
Creating Variables

	To create a variable, there are two steps:
	1. Declaration which allows to create the variable
	2. Initialization which allows to add an initial value to a variable

Variables in JS are declared with the use of 'let' or 'const' keyword
*/

let myVariable;

/*
	We can create variables without an initial value. However, when logged into the console
	the variable will return a value of undefined

	Undefined is a data type that indicates that variable does exist. However, there
	was no initial value
*/

console.log(myVariable);

myVariable = "New Initialized Value";
console.log(myVariable);

/*
myVariable2 = "Initial Value";
let myVariable2;
console.log(myVariable2);

	Note: You cannot and should not access a variable before it's been declared

	Can you use or refer to a variable that has not been declared? NO. This will
	result in an error
*/

/*
Undefined vs Not Defined
	Undefined means a variable has been decalred but there is no initial value
	- It is a data type

	Not defined means that the variable you are trying to refer or access does NOT exist
	- Not defined is an error

	Note: Some errors in JS will stop the program from further executing
*/

let myVariable3 = "Another sample";
console.log(myVariable3);

//Variables must be declared first before they are used, referred to, or accessed

/*
'let' vs 'const'

	With the use of let keyword, we can create variables that can be declared, initialized,
	and re-assigned

	We can declare let variables and initialize after
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);

//Re-assigning let variables
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

//The value changed. We can re-assign values to let variables

const pi = 3.1416;
console.log(pi);

/*
'const' variables are variables with constant data. Therefore, we should not re-declare

Can you re-assign another value to a const variable? NO. An error will occur
*/

/*
Guides on Variable Names
	1. When making variables, it is important to create variables that are descriptive
	and indicative of data it contains

		let firstName = "Sean" (Good variable name)
		let pokemon = 25000 (Bad variable name)

	2. When naming variables, it is better to start with a lowercase letter. We usually
	avoid creating variable names that starts with capital letter because there are 
	several keywords in JS that start with capital letter

		let FirstName = "Michael"; (Bad variable name)

	3. Do not add to your variable names. Use camelCase for multiple words or underscore

		let first name = "Mike"; (Bad variable name)

		camelCase: lastName, emailAddress, mobileNumber

		under_score: product_description
*/

let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);

//Declaring multiple variables

let brand = "Toyota", model = "Vios", type = "Sedan;"
console.log(brand);
console.log(model);
console.log(type);

//Console logging multiple variables: use commas to separate each variable
console.log(brand, model, type);

/* 
Data Types

	In most programming languages, data is differentiated by their types. For most
	programming languages, you have to declare not only the variable name, but also
	the type of data you are saving into a variable. However, JS does not require this

	To create data with particular data types, some data types require adding literals.

		string literals = '', "", most recently: ``(template literals)
		object literals = {}
		array literals = []
*/

/*
Strings are a series of alphanumeric that create a word, a phrase, a name, or anything
related to creating text

String literals such as '' (single quote) or "" (double quote) are used to write or 
create strings
*/

let country = "Philippines";
let province = 'Metro Manila';

console.log(country, province);

/*Mini Activity 1*/

let firstName = "Sean Patrick";
let lastName = "Serrano";

console.log(firstName, lastName);

/*
Concatenation is a process/operation wherein we combine two strings as one using the
plus (+) sign

In JS Strings, spaces are also counted as characters
*/

//Method 2
console.log(firstName + " " + lastName);

//Method 3
let fullName = firstName + " " + lastName
console.log(fullName);

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmariñas";
let space = " ";

/*Mini-Activity 2*/

let sentence = fullName + space + word1 + space + word6 + space 
				+ word2 + space + word3 + space + word5 + space
			    + word4 + space + word7;
console.log(sentence);

//Method 2
let sentence2 = `${fullName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word4} ${word7}`;
console.log(sentence2);   

/*
Template Literals (``) allow us to create string with the use of backticks. Template
literals also allow us to easily concatenate strings without the use of plus (+)

This also allows to embed or add variables and even expression in our string with
the use of placeholders ${}
*/

/*
Number (Data Type)
	
	Integers (whole numbers) and floats (decimals) are our number data which can be
	used for mathematical operations
*/

let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;

console.log(numString1 + numString2);	//Strings were concatenated
console.log(num1 + num2);				//Integers were added

let num3 = 5.5;
let num4 = .5;

console.log(num1 + num3);
console.log(num3 + num4);

/*
When the + or addition operator is used on numbers, it will do the proper mathematical
operation. However, if used on strings, it will concatenate.
*/

console.log(numString1 + num1);			// 55
console.log(num3 + numString2);			// 5.56

//Forced coercion - when one data's type is forced to change to complete an operation
//string + num = concatenation

console.log(parseInt(numString1) + num1);

//numString1 was parsed into a proper number